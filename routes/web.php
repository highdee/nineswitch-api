<?php

use Illuminate\Support\Facades\Route;

Route::get('/mail', function () {
    return view('mails.resetPassword');
});
