<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController\UserAuthController;
use App\Http\Controllers\UserController\UserController;
use \App\Http\Controllers\VerificationController;
use \App\Http\Controllers\WalletController;

Route::prefix('v1')->group(function(){
    Route::post('create-account', [UserAuthController::class, "registration"]);
    Route::post('login', [UserAuthController::class, "login"]);
    Route::post('verify-email', [UserAuthController::class, "verify_email"]);
    Route::post('forgot-password', [UserAuthController::class, "forgot_password"]);
    Route::post('reset-password', [UserAuthController::class, "reset_password"]);
    Route::post('logout', [UserAuthController::class, "logout"]);

    Route::post('payment-hook',[WalletController::class, "receivePaymentHook"]);
});

Route::prefix('v1')->group(function(){
    Route::middleware('jwt.auth')->group(function(){
        Route::get('get-user', [UserController::class, "getUser"]);
        Route::post('send-token', [UserController::class, "sendMobileToken"]);
        Route::post('verify-phone', [UserController::class, "verifyPhone"]);

        Route::prefix('verification')->group(function (){
            Route::post('bvn', [VerificationController::class , 'addBvn']);
            Route::post('upload-id', [VerificationController::class , 'uploadID']);
            Route::post('upload-selfie', [VerificationController::class , 'uploadSelfie']);
        });

        Route::post('update-profile', [UserController::class, "updateProfile"]);

        Route::prefix('transactions')->group(function (){
            Route::get('get', [\App\Http\Controllers\TransactionController::class , 'getTransactions']);
        });
        Route::prefix('top-up')->group(function (){
            Route::get('billers', [\App\Http\Controllers\TopupController::class , 'getBillers']);
            Route::post('bill', [\App\Http\Controllers\TopupController::class , 'topup']);
        });


        Route::prefix('top-up')->group(function (){
            Route::get('billers', [\App\Http\Controllers\TopupController::class , 'getBillers']);
            Route::post('bill', [\App\Http\Controllers\TopupController::class , 'topup']);
        });
    });

});

