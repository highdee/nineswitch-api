<?php
namespace App\Services;

use Illuminate\Support\Facades\Http;
class HttpServices {

    public static function post($url, $payload, $headers=[]){
        $response = Http::withHeaders($headers)->post($url, $payload);
        return $response;
    }

    public static function get($url, $headers=[]){
        $response = Http::withHeaders($headers)->get($url);
        return $response;
    }
}
