<?php
namespace App\Services;

use App\Mail\sendEmailVerification;
use App\Models\User;
use App\Models\UserVerification;
use App\Models\Wallet;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class UserServices{
    public static function sendVerificationToken($user_id){
        $user = User::find($user_id);
        if($user){
            if($user->next_token_at < 60 && $user->next_token_at > 1){
                return;
            }
            $user->remember_token = mt_rand(10000, 99999);
            $user->save();
//            dispatch(function ($user){
//                    TwilioServices::sendSms($user->phone, $user->remember_token);
//                })->afterResponse();

            $UserVerify = new UserVerification();
            $UserVerify->user_id = $user->id;
            $UserVerify->type = 'Phone';
            $UserVerify->value = $user->remember_token;
            $UserVerify->save();

        }
    }

    public static function sendForgotToken($user_id){
        $user = User::find($user_id);
        if($user){
            if($user->next_token_at < 60){
                return;
            }
            $user->remember_token =mt_rand(10000, 99999);
            $user->save();
//            dispatch(function ($user){
//                    TwilioServices::sendSms($user->phone, $user->remember_token);
//                })->afterResponse();

            $UserVerify = new UserVerification();
            $UserVerify->user_id = $user->id;
            $UserVerify->type = 'Forgot Token';
            $UserVerify->value = $user->remember_token;
            $UserVerify->save();

        }
    }

    public static function sendEmailVerificationToken($user_id){
        $user = User::find($user_id);

        if($user){
            $user->remember_token = bin2hex(random_bytes(4));
            $user->save();

            try {
                Mail::to($user->email)->send(new sendEmailVerification([
                    "name" => $user->firstname,
                    'auth_code' => $user->remember_token
                ]));
                return ResponseServices::CustomResponse(true, "Enter the confirmation code sent to your email to validate your email");
            }catch (\Exception $e){
                return ResponseServices::CustomResponse(false, "There is an error processing your request, please check back later");
            }
        }
    }


    public static function creditWallet(
        $user_id,
        $wallet_type,
        $amount=0,
        $description=null,
        $gateway=null,
        $gateway_ref=null
    ){
        $wallet = Wallet::where([
            'user_id'=>$user_id,
            'type'=>$wallet_type
        ])->first();

        if(!$wallet) return false;

        if(is_null($description)){
            $description = "You have received a sum of ".number_format($amount)." ".$wallet_type;
        }
        $balance_before = $wallet->amount;
//        Credit Wallet
        $wallet->amount += $amount;
        $wallet->save();

//        Create Transaction
        $txn = new Transaction();
        $txn->user_id = $user_id;
        $txn->amount = $amount;
        $txn->description = $description;
        $txn->type = 1;
        $txn->division = '';
        $txn->wallet_type = $wallet_type;
        $txn->gateway = $gateway;
        $txn->gateway_ref = $gateway_ref;
        $txn->wallet_id = $wallet->id;
        $txn->balance_before = $balance_before;
        $txn->balance_after = $wallet->amount;
        $txn->reference = mt_rand(10000, 99999999).'-'.date('ymdhis');
        $txn->save();


        //Notify user

        return true;
    }

    public static function debitWallet($user_id, $wallet_type, $amount=0, $description=null){
        $wallet = Wallet::where([
            'user_id'=>$user_id,
            'type'=>$wallet_type
        ])->first();

        if(!$wallet) return false;

        if(is_null($description)){
            $description = "You have been debited a sum of ".number_format($amount)." ".$wallet_type;
        }
        $balance_before = $wallet->amount;
//        Credit Wallet
        $wallet->amount -= $amount;
        $wallet->save();

//        Create Transaction
        $txn = new Transaction();
        $txn->user_id = $user_id;
        $txn->amount = $amount;
        $txn->description = $description;
        $txn->type = -1;
        $txn->division = '';
        $txn->wallet_type = $wallet_type;
        $txn->wallet_id = $wallet->id;
        $txn->balance_before = $balance_before;
        $txn->balance_after = $wallet->amount;
        $txn->reference = mt_rand(10000, 99999999).'-'.date('ymdhis');
        $txn->save();
        return true;
    }
}
