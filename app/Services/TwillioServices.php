<?php
namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception;
use Twilio\Rest\Client;

class TwilioServices    {
    public static function sendSms($phone_number, $token){

        //phone verification
        $receiverNumber = strval($phone_number);
        $message = "Verification token for your Nineswitch account is  ".$token;
        try {
            $account_sid = env("TWILIO_SID");
            $auth_token = env("TWILIO_TOKEN");
            $twilio_number = getenv("TWILIO_FROM");

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message]);
        } catch (Exception $e) {

        }

    }
}
