<?php
namespace App\Services;

class ResponseServices{

    public static function CustomResponse($status, $message, $status_code=200, $data= []){
        return response([
            'status' => $status,
            'message'=> $message,
            'data'=> $data
        ], $status_code);
    }
}


