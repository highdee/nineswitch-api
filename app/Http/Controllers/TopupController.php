<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Wallet;
use App\Services\ResponseServices;
use App\Services\UserServices;
use Illuminate\Http\Request;
use App\Services\HttpServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TopupController extends Controller
{

    public function getBillers(Request $request){
        $headers = ['Authorization'=>'Bearer '.env('FLUTTER_SECRET_KEY')];

        $response = HttpServices::get('https://api.flutterwave.com/v3/bill-categories', $headers);
        if($response->status() == 200){
            $data = $response->object();
            $collection = collect($data->data);

            if($request->query('type') == 'airtime'){
                $airtime_billers = $this->getAirtimeBillers($collection);
                return response([
                    'status'=> true,
                    'data'=>$airtime_billers
                ], 200);
            }
            else if($request->query('type') == 'data'){
                $_billers = $this->getDataBillers($collection);

                return response([
                    'status'=> true,
                    'data'=>$_billers
                ], 200);
            }
            else if($request->query('type') == 'cable'){
                $_billers = $this->getCableBillers($collection);

                return response([
                    'status'=> true,
                    'data'=>$_billers
                ], 200);
            }
            else if($request->query('type') == 'electricity'){
                $_billers = $this->getElectricityBillers($collection);

                return response([
                    'status'=> true,
                    'data'=>$_billers
                ], 200);
            }
            return response($collection);
        }
        return response([
            'status'=>false,
            'message'=>'Service is currently unavailable'
        ], 400);
    }

    public function getAirtimeBillers($collection){
        $airtime_billers = $collection
            ->where('is_airtime','=',true)
            ->where('country','=','NG')
            ->unique('short_name');
        return $airtime_billers;
    }

    public function getDataBillers($collection) {
        $data_billers = $collection
            ->where('is_airtime', '=', false)
            ->where('country', '=', 'NG')
            ->where('label_name', '=', 'Mobile Number')
            ->unique('biller_code');

        $_billers =  [];
        foreach ($data_billers as $biller) {
            $bundles = $collection
                ->where('is_airtime', '=', false)
                ->where('country', '=', 'NG')
                ->where('label_name', '=', 'Mobile Number')
                ->where('biller_code', '=', $biller->biller_code);

            $temp = [
                'short_name' => explode(' ', $biller->name)[0],
                'biller_name' => $biller->biller_name,
                'data' => []
            ];

            foreach ($bundles as $bundle) {
                $temp['data'][] = $bundle;
            }
            $_billers[] =  $temp;
        }

        return $_billers;
    }

    public function getCableBillers($collection) {
        $data_billers = $collection
            ->where('is_airtime', '=', false)
            ->where('country', '=', 'NG')
            ->where('label_name', '=', 'SmartCard Number')
            ->unique('biller_code');

        $_billers =  [];
        foreach ($data_billers as $biller) {
            $bundles = $collection
                ->where('is_airtime', '=', false)
                ->where('country', '=', 'NG')
                ->where('label_name', '=', 'SmartCard Number')
                ->where('biller_code', '=', $biller->biller_code);

            $temp = [
                'short_name' => explode(' ', $biller->name)[0],
                'biller_name' => $biller->biller_name,
                'data' => []
            ];

            foreach ($bundles as $bundle) {
                $temp['data'][] = $bundle;
            }
            $_billers[] =  $temp;
        }

        return $_billers;
    }

    public function getElectricityBillers($collection) {
        $_billers = collect($collection)
            ->where('is_airtime','=',false)
            ->where('country','=','NG')
            ->where('label_name', '=', 'Meter Number')
            ->unique('short_name')
;

        $result = [
            "postpaid"=>[],
            "prepaid"=>[]
        ];

        foreach ($_billers as $key => $biller){
            $bl = $_billers[$key];
            if(str_contains($bl->biller_name,'POSTPAID')){
                $result['postpaid'][] = $biller;
            }else{
                $result['prepaid'][] = $biller;
            }
        }
        return [
            [
                "label"=>'postpaid',
                "data"=> $result['postpaid']
            ],
            [
                "label"=>'prepaid',
                "data"=> $result['prepaid']
            ],
        ];
    }

    public function topup(Request $request){
        $validator = Validator::make( $request->all(), [
            'phone' => 'required',
            'biller' => 'required',
            'amount' => 'required',
        ]);

        if($validator->fails()){
            $message = $validator->errors();
            return ResponseServices::CustomResponse(false, "Validation error", 422, json_encode($message));
        }


        $user = Auth::user();

        $wallet = Wallet::where('user_id', $user->id)->
                            where('type', 'naira')->first();

        if(!$wallet){
            return response([
                'status'=>false,
                'message' => "your naira wallet wasn't found."
            ], 400);
        }

        if($wallet->amount < $request->amount){
            return response([
                'status'=>false,
                'message' => "Insufficient balance in your naira wallet"
            ], 400);
        }


        $headers = ['Authorization'=>'Bearer '.env('FLUTTER_SECRET_KEY')];

        $payload = [
            "country"=> "NG",
            "customer"=> $request->phone,
            "amount"=> $request->amount,
            "recurrence"=> "ONCE",
            "type"=> $request->biller,
            "reference"=> date('Ymdhis').'-'.mt_rand(1000, 99999)
        ];

        $response = HttpServices::post(
            'https://api.flutterwave.com/v3/bills',
            $payload,
            $headers
        );



        if($response->status() == 200){
            // charge customer
            UserServices::debitWallet($user->id, 'naira', $request->amount, 'Mobile Topup was successfull');

            return response([
                'status'=>true,
                'message'=>'Topup successfully',
                'response'=> $response->object(),
                'user'=>User::find($user->id)
            ], 200);
        }

        return response([
            'status'=>false,
            'message'=>$response->object()->message, //'Service is currently unavailable',
//            'response'=> $response->object()->message
        ], 400);
    }
}
