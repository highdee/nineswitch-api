<?php

namespace App\Http\Controllers\UserController;

use App\Http\Controllers\Controller;
use App\Mail\resetPassword;
use App\Services\UserServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use JWTAuth;
use Auth;
use App\Models\User;
use PHPUnit\Exception;
use App\Services\ResponseServices;
use App\Services\TwilioServices;

class UserAuthController extends Controller
{
    public function login(Request $request)
    {
        $validation_list = [
            'phone' => 'required',
            'password' => 'required'
        ];
        if($request->input('platform') === 'web'){
            $validation_list['phone'] = 'nullable';
            $validation_list['email'] = 'required|email';
        }

        $validator = Validator::make( $request->all(), $validation_list);
        if($validator->fails()){
            $message = $validator->errors();
            return ResponseServices::CustomResponse(false, "Validation error", 422, json_encode($message));
        }

        if ($request->input('platform') === 'web'){
            $credentials = $request->only('email', 'password');
        }else{
            $credentials = $request->only('phone', 'password');
        }

        if ($token = JWTAuth::attempt($credentials)) {
            $user = User::where('id', auth::user($token)->id)->first();

            //Send Phone verification
            if($request->input('platform') != 'web' && is_null($user->phone_verified_at) ){
                UserServices::sendVerificationToken($user->id);
            }

            return ResponseServices::CustomResponse(true, "", 200, [
                'status' => true,
                'message' => "Login successfull",
                'token' => $token,
                "user" => $user
            ]);
        } else {
           return ResponseServices::CustomResponse(false, "Email/Phone or Password is not correct.", 400, []);
        }
    }

    public function registration(Request $request )
    {
        $validation_list = [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users',
            'password' => 'required|confirmed|min:6',
        ];
        if($request->input('platform') == 'mobile'){
            $validation_list['password'] = 'required|min:6';
        }
        $validator = Validator::make( $request->all(), $validation_list);

        if($validator->fails()){
           $message = $validator->errors();
           return ResponseServices::CustomResponse(false, "Validation error", 422, json_encode($message));
        }

        try {
            $user = new User();
            $user->firstname = $request['firstname'];
            $user->lastname = $request['lastname'];
            $user->email = $request['email'];
            $user->phone = $request['phone'];
            $user->unique_id = substr(sha1(time()), 0, 6);
            $user->password = bcrypt($request['password']);
            $user->save();

            $credentials = $request->only('email', 'password');
            if ($token = JWTAuth::attempt($credentials)) {
                $user = User::where('id', auth::user($token)->id)->first();

                if($request->platform == 'mobile'){
                    //Send Phone verification
                    UserServices::sendVerificationToken($user->id);
                }else{
                    UserServices::sendEmailVerificationToken($user->id);
                }

                return ResponseServices::CustomResponse(true, "Registration successful", 200,
                    [ 'token' => $token, "user" => $user ]
                );
            } else {
                return ResponseServices::CustomResponse(false, "Registration unsuccessful");
            }
        }
        catch (Exception $exception){
            return ResponseServices::CustomResponse(false, '', 500, $exception->getMessage());
        }
    }

    public function forgot_password(Request $request)
    {
        if($request->platform == "mobile"){
            $user = User::where('phone', $request->input('phone'))->first();
        }else{
            $user = User::where('email', $request->input('email'))->first();
        }

        if (!$user) {
            return ResponseServices::CustomResponse( false, "This Email/Phone number is not registered with us.",400);
        }

        $user->remember_token = bin2hex(random_bytes(4));
        $user->save();

        $auth_code = $user->remember_token;
        try {
            if($request->platform == 'mobile'){
                //Send Phone verification
                UserServices::sendForgotToken($user->id);
            }else{
                //Send Web verification
                Mail::to($user->email)->send(new resetPassword([
                    "name" => $user->firstname,
                    'auth_code' => $auth_code
                ]));
            }
            return ResponseServices::CustomResponse(true, "Enter the security code sent to your email to reset your password");
        }catch (\Exception $e){
             return ResponseServices::CustomResponse(false, "There is an error processing your request, please check back later");//"Code cannot be sent at this moment, check back later"
        }
    }

    public function verify_email(Request $request)
    {
        $token = $request->code;
        if (!$token) {
            return ResponseServices::CustomResponse(false,"Verification code not found", 400 );
        }

        $user = User::where('remember_token', $token)->first();
        if (!$user) {
            return ResponseServices::CustomResponse(false,"User not found", 400 );
        }

        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $user->save();

        return ResponseServices::CustomResponse(true,"Email has been successfully verified" );
    }

    public function reset_password(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'password' => 'required|confirmed',
            'token' => 'required'
        ]);

        if($validator->fails()){
            $message = $validator->errors();
            return ResponseServices::CustomResponse(false, "Validation error", 422, json_encode($message));
        }

        $user = User::where('remember_token', $request->input('token'))->first();
        if (!$user) {
            return ResponseServices::CustomResponse(false, "The reset code provided is recognised, please try again.", 400);
        }

        $user->password = bcrypt($request->input('password'));
        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $user->save();

        return ResponseServices::CustomResponse(true,
            "Your password has been reset successfully, please login with your new password", 200);
    }

    public function logout()
    {
        auth::logout();
        return ResponseServices::CustomResponse(true, "Logged out successfully");
    }
}
