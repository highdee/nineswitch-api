<?php

namespace App\Http\Controllers\UserController;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\HelperController;
use App\Models\User;
use App\Services\ResponseServices;
use App\Services\UserServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Auth;
use PHPUnit\Exception;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
    }

    public function getUser()
    {
        $user = auth()->user();
        if($user) {
            return response()->json([
                "status" => true,
                'user' => $user
            ]);
        }

        return HelperController::returnMessage(false, "Access token has expired, please login again");
    }

    public function sendMobileToken(Request $request)
    {
        $user = Auth()->user();

        $validator = Validator::make( $request->all(), [
            'phone' => 'required',
        ]);

        if($validator->fails()){
            $message = $validator->errors();
            return ResponseServices::CustomResponse(false, "Validation error", 422, json_encode($message));
        }

        try {
            $check = User::where([
                'phone'=> $request->phone,
            ])->where('id','!=',$user->id)->count();
            if($check > 0){
                return ResponseServices::CustomResponse(
                    false,
                    "Another customer exist with this phone number",
                    422,
                    []);
            }

//            $user->phone = $request['phone'];
            //Send Phone verification
            UserServices::sendVerificationToken($user->id);

            return ResponseServices::CustomResponse(true, "Token sent successfully", 200,
                [
                    "user" => $user
                ]
            );
        }
        catch (Exception $exception){
//            return
        }
    }

    public function verifyPhone(Request $request){
        $user = Auth()->user();
        $validator = Validator::make( $request->all(), [
            'phone' => 'required',
            'code' => 'required',
        ]);

        if($validator->fails()){
            $message = $validator->errors();
            return ResponseServices::CustomResponse(false, "Validation error", 422, json_encode($message));
        }

        $check = User::where([
            'remember_token' => $request->code,
            'id'=> $user->id,
        ])->first();

        if(!$check){
            return ResponseServices::CustomResponse(false, 'Invalid token', 400, []);
        }

        $check->phone_verified_at = date('Y-m-d H:i:s');
        $check->phone = $request->phone;
        $check->remember_token = 0;
        $check->save();

        return ResponseServices::CustomResponse(
            true,
            'Phone number was verified successfully',
            200,
            [
                'user'=> User::find($user->id)
            ]
        );
    }

    public function updateProfile(Request $request){
        $user = User::where('id', Auth::user($request->token)->id)->first();

        if (isset($request['email'])) {
            $user->email = $request->email;
        }
        if (isset($request['firstname'])) {
            $user->firstname = $request->firstname;
        }
        if (isset($request['lastname'])) {
            $user->lastname = $request->lastname;
        }
        if (isset($request['gender'])) {
            $user->gender = $request->gender;
        }
        if (isset($request['date_of_birth'])) {
            $user->date_of_birth = $request->date_of_birth;
        }
        if (isset($request['phone'])) {
            $user->phone = $request->phone;
        }
        if (isset($request['country'])) {
            $user->country = $request->country;
        }
        if (isset($request['state'])) {
            $user->state = $request->state;
        }
        if (isset($request['city'])) {
            $user->city = $request->city;
        }
        if (isset($request['address'])) {
            $user->address = $request->address;
        }
        if (isset($request['zip_code'])) {
            $user->zip_code = $request->zip_code;
        }

        if (!$user->save()) {
            return response([
                'status' => false,
                'msg' => 'Error updating profile, pls try again',

            ]);
        }

        return response([
            'status' => true,
            'msg' => 'Profile updated successfully',
            'user' => User::find($user->id)
        ]);
    }
}
