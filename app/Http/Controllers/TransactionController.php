<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    //
    public function getTransactions(Request $request){
        $user = Auth::user();
        $limit = 15;

        $txns = Transaction::where('user_id', $user->id);

        //TYPE
        if($request->has('type')){
            $txns = $txns->where('wallet_type', $request->query('type'));
        }
        //START DATE
        if($request->has('start_date')){
            $start_date = date('Y-m-d', strtotime($request->query('start')));
            $txns = $txns->whereDate('created', '>=', $start_date);
        }
        //END DATE
        if($request->has('end_date')){
            $end_date = date('Y-m-d', strtotime($request->query('end')));
            $txns = $txns->whereDate('created', '<=', $end_date);
        }
        //LIMIT
        if($request->has('limit')){
//            $txns = $txns->limit($request->query('limit'));
            $limit = $request->has('limit');
        }


        if(!$request->has('start') && !$request->has('end') && !$request->has('limit')){
            $txns = $txns->limit(10);
        }

        $txns = $txns->with('user')->orderBy('id', 'desc')->paginate($limit);

        return response($txns, 200);
    }
}
