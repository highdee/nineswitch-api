<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserVerification;
use App\Services\ResponseServices;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function addBvn(Request $request){
        $request->validate([
            'bvn' => ['required'],
        ]);

        $user = Auth()->user();

        $UserVerify = new UserVerification();
        $UserVerify->user_id = $user->id;
        $UserVerify->type = 'BVN';
        $UserVerify->value = $request->bvn;
        $UserVerify->save();

        return ResponseServices::CustomResponse(
            true,
            "Bvn details was updated successfully",
            200,
            [
                'user'=> User::find($user->id)
            ]
        );
    }

    public function uploadID(Request $request){
        $user = Auth()->user();

        if(!$request->input("file") || !$request->input('filename')) {
            return ResponseServices::CustomResponse(
                false,
                "Document not found",
                400
            );
        }


        $file = $request->input('file');
        $file = base64_decode($file);
        $extension = explode('.', $request->input('filename'));
        $imagename = date('dmyhis') . mt_rand(0, 99999) . '.' . $extension[count($extension)-1];

        file_put_contents(storage_path() . "/document_ids/" . $imagename, $file);
        $filename = env('APP_URL'). "/view/document_ids/" . $imagename;

        $UserVerify = new UserVerification();
        $UserVerify->user_id = $user->id;
        $UserVerify->type = 'ID';
        $UserVerify->value = $filename;
        $UserVerify->save();

        return ResponseServices::CustomResponse(
            true,
            "ID was uploaded successfully successfully",
            200,
            [
                'user'=> User::find($user->id)
            ]
        );
    }

    public function uploadSelfie(Request $request){
        $user = Auth()->user();

        if(!$request->input("file") || !$request->input('filename')) {
            return ResponseServices::CustomResponse(
                false,
                "Document not found",
                400
            );
        }


        $file = $request->input('file');
        $file = base64_decode($file);
        $extension = explode('.', $request->input('filename'));
        $imagename = date('dmyhis') . mt_rand(0, 99999) . '.' . $extension[count($extension)-1];

        file_put_contents(storage_path() . "/document_ids/" . $imagename, $file);
        $filename = env('APP_URL'). "/view/document_ids/" . $imagename;

        $UserVerify = new UserVerification();
        $UserVerify->user_id = $user->id;
        $UserVerify->type = 'Selfie';
        $UserVerify->value = $filename;
        $UserVerify->save();

        return ResponseServices::CustomResponse(
            true,
            "Photo was uploaded successfully successfully",
            200,
            [
                'user'=> User::find($user->id)
            ]
        );
    }
}
