<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Wallet;
use App\Services\UserServices;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    public function receivePaymentHook(Request  $request){
        $details = $request->input();
        $details = $details['data'];
        //rideeats-hook
        if(!$request->headers->has('verif-hash')){
            return response('', 401);
        }
        if($request->headers->get('verif-hash') != 'nineswitch-hook'){
            return response('', 401);
        }

        try {
            //Payment wasn't succcessfull

            if (!in_array(strtolower($details['status']), ['successful', 'successful'])) {
                return response(['Not successful'], 200);
            }

            $reference = '';
            if (isset($details['tx_ref'])) {
                $reference = $details['tx_ref'];
            } else if (isset($details['reference'])) {
                $reference = $details['reference'];
            }

            $reference_splitted = explode('-', $reference);
            $uuid = $reference_splitted[0];
            $txn_ref = $reference_splitted[1].'-'.$reference_splitted[2];


            //check transactions if it already exist



            $user = User::where('unique_id', $uuid)->first();

            //Log this error
            if(!$user) return response($user, 200);


            UserServices::creditWallet(
                $user->id,
                'naira',
                $details['amount'],
                null,
                "Flutterwave",
                $details['id']
            );

            return $user;
        }
        catch (\Exception $exception){
            return response($exception, 200);
        }
        return response([], 200);
    }
}
