<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    public $hidden = ['user_id'];
    public $casts = [
        'created_at'=>'datetime: M d, Y H:iA'
    ];
    protected $appends = ['amount_formatted'];

    protected function getAmountFormattedAttribute(){
        $amount =  number_format($this->amount,2);
        if($this->type < 0 ){
            $amount = '-'.$amount;
        }
        return $amount;
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
