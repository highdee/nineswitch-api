<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject {
    use HasFactory, Notifiable;

    protected $fillable = [
        'email',
        'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['next_token_at', 'bvn', 'document', 'selfie', 'wallet'];

    public function getWalletAttribute(){
        $wallets =  [];

        foreach ($this->wallets()->get() as $wallet){
            $wallets[$wallet->type]  = number_format($wallet->amount,0);
        }

        return $wallets;
    }

    public function getSelfieAttribute(){
        $document = null;
        $lastSent = UserVerification::where('user_id', $this->id)->where('type', 'Selfie')->orderBy('id', 'desc')->first();
        if($lastSent){
            $document = $lastSent->value;
        }
        return $document;
    }

    public function getDocumentAttribute(){
        $document = null;
        $lastSent = UserVerification::where('user_id', $this->id)->where('type', 'ID')->orderBy('id', 'desc')->first();
        if($lastSent){
            $document = $lastSent->value;
        }
        return $document;
    }

    public function getBvnAttribute(){
        $bvn = null;
        $lastSent = UserVerification::where('user_id', $this->id)->where('type', 'BVN')->orderBy('id', 'desc')->first();
        if($lastSent){
            $bvn = $lastSent->value;
        }
        return $bvn;
    }

    public function getNextTokenAtAttribute(){
        $seconds=-1;
        $lastSent = UserVerification::where('user_id', $this->id)->where('type', 'Phone')->orderBy('id', 'desc')->first();
        if($lastSent){
            $diff = strtotime(date('Y-m-d H:i:s'))-strtotime($lastSent->created_at);
            $seconds = $diff;
        }
        return $seconds;
    }

    public function wallets(){
        return $this->hasMany('App\Models\Wallet');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
