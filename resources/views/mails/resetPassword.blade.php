<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,600" rel="stylesheet" type="text/css">
    <style>
        * {
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,600" rel="stylesheet" type="text/css">
    <style>
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
            font-family: 'Roboto', sans-serif !important;
            font-size: 14px;
            margin-bottom: 10px;
            line-height: 24px;
            color:#8094ae;
            font-weight: 400;
        }
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
        }
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }
        a {
            text-decoration: none;
        }
        img {
            -ms-interpolation-mode:bicubic;
        }
    </style>

</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f5f6fa;">
    <center style="width: 100%; background-color: #f5f6fa;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f5f6fa">
        <tr>
            <td style="padding: 40px 0;">
                <table style="width:100%;max-width:620px;margin:0 auto;">
                    <tbody>
                    <tr>
                        <td style="text-align: center; padding-bottom:25px">
                            <a>
                                <svg id="Layer_1" width="150px" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 109.1 16.32"><defs><style>.cls-1{fill:#4467ae;}.cls-2{fill:#ee2f5c;}</style></defs><title>Untitled-3</title><path class="cls-1" d="M5.43,16.64V2.14H9c3.59,8,4.29,9.54,4.49,10.25h0c-.15-1.74-.15-3.87-.15-6.25v-4h2.78v14.5H12.73C9,8.1,8.35,6.56,8.13,5.89h0c.13,1.79.13,3.92.13,6.52v4.23Z" transform="translate(-5.43 -1.41)"/><path class="cls-1" d="M18.82,5.55h3.07V16.64H18.82Zm0-4.14h3.07V3.94H18.84Z" transform="translate(-5.43 -1.41)"/><path class="cls-1" d="M24.63,9c0-2.28,0-2.87,0-3.45h3a8.67,8.67,0,0,1,.09,1.15,3.22,3.22,0,0,1,2.94-1.36A3.05,3.05,0,0,1,34,8.73v7.91H30.92V9.31c0-1.1-.26-1.78-1.5-1.78s-1.72.55-1.72,2.24v6.87H24.63Z" transform="translate(-5.43 -1.41)"/><path class="cls-1" d="M39.16,11.76c0,1.41.21,3,1.91,3a1.58,1.58,0,0,0,1.78-1.46h2.94c0,.35-.35,3.58-4.77,3.58s-4.91-3-4.91-5.83c0-3.52,1.6-5.67,5-5.67,3.86,0,4.81,2.42,4.81,5.31a10.17,10.17,0,0,1,0,1.1Zm3.7-1.85c0-1.08-.15-2.45-1.78-2.45S39.21,9.1,39.21,9.91Z" transform="translate(-5.43 -1.41)"/><path class="cls-2" d="M50.25,13.28A1.57,1.57,0,0,0,52,14.88c1,0,1.5-.44,1.5-1.17s-.42-1.21-2-1.7c-3.14-1-3.92-1.88-3.92-3.49s1.14-3.18,4.32-3.18c3.49,0,4.34,2,4.34,3.45H53.29a1.34,1.34,0,0,0-1.48-1.5c-.81,0-1.25.43-1.25,1s.48,1,2.07,1.52c2.82.87,3.88,1.76,3.88,3.55,0,2.19-1.63,3.43-4.65,3.43-3.18,0-4.6-1.57-4.61-3.55Z" transform="translate(-5.43 -1.41)"/><path class="cls-2" d="M60.46,5.55c1,4.88,1.33,6.89,1.52,8.34h0c.22-1.49.78-3.13,2-8.34h2.76c1,4.16,1.74,7,1.9,8.31h0c.18-1.16.44-2.73,1.62-8.31h3.06L70.11,16.64H67c-.8-3.38-1.46-6-1.72-7.6h0c-.23,1.58-1,4.3-1.92,7.6H60.18l-3-11.09Z" transform="translate(-5.43 -1.41)"/><path class="cls-2" d="M85.66,5.9h1.76V3.19h3.1V5.9H92.6V8H90.52V13.9c0,.63.15,1,1.11,1a4.6,4.6,0,0,0,.77,0v1.84a6.82,6.82,0,0,1-1.9.22c-2.15,0-3.08-.64-3.08-2.59V8H85.66Z" transform="translate(-5.43 -1.41)"/><path class="cls-2" d="M103.16,13.08c0,.88-.29,3.87-4.74,3.87-3.85,0-4.81-2.49-4.81-5.71,0-2.88,1.18-5.54,5-5.54,4.42,0,4.56,3.07,4.58,3.73H100c0-.42-.08-1.66-1.47-1.66S96.75,9,96.75,11.26s.43,3.62,1.77,3.62,1.56-1.18,1.64-1.8Z" transform="translate(-5.43 -1.41)"/><path class="cls-2" d="M108.17,1.86V7a3.35,3.35,0,0,1,3-1.25c2.22,0,3.34,1.17,3.34,3.39v7.67h-3.1V9.59c0-1.06-.24-1.74-1.55-1.74s-1.71.84-1.71,2.27v6.64h-3.11V1.86Z" transform="translate(-5.43 -1.41)"/><path class="cls-1" d="M81.37,5.57V7.14a4.66,4.66,0,1,1-4,.19V5.71a6.13,6.13,0,1,0,4-.14Z" transform="translate(-5.43 -1.41)"/><rect class="cls-2" x="73.01" y="3.75" width="1.96" height="6.23" rx="0.98"/><ellipse class="cls-2" cx="73.93" cy="2.02" rx="0.82" ry="0.84"/></svg>
                            </a>
                            <p style="font-size: 14px; color: #6576ff; padding-top: 12px;"></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table style="width:100%;max-width:620px;margin:0 auto;background-color:#ffffff;">
                    <tbody>
                    <tr>
                        <td style="text-align:center;padding: 30px 30px 15px 30px;">
                            <h2 style="font-size: 18px; color: #6576ff; font-weight: 600; margin: 0;">Reset Password</h2>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;padding: 0 30px 20px">
                            <p style="margin-bottom: 10px; font-size: 20px">Hi {{ $name }},</p>
                            <p style="margin-bottom: 25px;">The authentication code to reset your password can be found below.</p>
                            <span style="background-color:#6576ff;border-radius:4px;color:#ffffff;display:inline-block;font-size:20px;letter-spacing:4px;font-weight:normal;line-height:44px;text-align:center;text-decoration:none; padding: 0 25px">{{ $auth_code }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;padding: 20px 30px 40px">
                            <p>If you did not make this request, please contact us or ignore this message.</p>
                            <p style="margin: 0; font-size: 13px; line-height: 22px; color:#9ea8bb;">This is an automatically generated email please do not reply to this email. If you face any issues, please contact us at  help@nineswitch.com</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table style="width:100%;max-width:620px;margin:0 auto;">
                    <tbody>
                    <tr>
                        <td style="text-align: center; padding:25px 20px 0;">
                            <p style="font-size: 13px;">Copyright © {{ date("Y") }} Nineswitch. All rights reserved. <br><a style="color: #6576ff; text-decoration:none;">Nineswitch</a>.</p>
                            <ul style="margin: 10px -4px 0;padding: 0;">
                                <li style="display: inline-block; list-style: none; padding: 4px;"><a style="display: inline-block; height: 30px; width:30px;border-radius: 50%; background-color: #ffffff" href="#"><img style="width: 30px" src="https://www.freepnglogos.com/uploads/facebook-logo-icon/facebook-logo-clipart-flat-facebook-logo-png-icon-circle-22.png" alt="brand"></a></li>
                                <li style="display: inline-block; list-style: none; padding: 4px;"><a style="display: inline-block; height: 30px; width:30px;border-radius: 50%; background-color: #ffffff" href="#"><img style="width: 30px" src="https://www.freepnglogos.com/uploads/instagram-logo-png-transparent-0.png" alt="brand"></a></li>
                                <li style="display: inline-block; list-style: none; padding: 4px;"><a style="display: inline-block; height: 30px; width:30px;border-radius: 50%; background-color: #ffffff" href="#"><img style="width: 30px" src="https://www.freepnglogos.com/uploads/twitter-logo-png/twitter-bird-symbols-png-logo-0.png" alt="brand"></a></li>
                            </ul>
                            <p style="padding-top: 15px; font-size: 12px;">This email was sent to you as a registered user of <a style="color: #6576ff; text-decoration:none;" href="https://softnio.com">Nineswitch</a>. To update your emails preferences <a style="color: #6576ff; text-decoration:none;" href="#">click here</a>.</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    </center>
</body>
</html>
